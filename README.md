# アクセスランキング機能付きアンテナサイト

参考サイト : http://2chmatomeru.info/

## 起動

```bash
$ git clone git@bitbucket.org:d-mato/access-ranking.git
$ cd access-ranking/dist

# DB作成
$ sqlite3 rss.db < ../dump.sql

# PHPビルトインウェブサーバー起動
$ php -S localhost:12345

```
ブラウザ起動

- 管理画面: admin.php
- トップページ: index.php

```
# 記事リストの更新
$ php update.php
```


## 管理画面

RSSの登録・削除ができる


## 自動更新

登録されたRSSを定期的に取得し、DBを更新する


## トップページ

基本的に記事を新着順に並び替えてリスト表示する

リンクのクリック数を集計しておき、並び順に反映させる
(重み付けの調整が必要？)

## データベーススキーマ

### rss

``id int, rss_url varchar, site_name varchar, created_at datetime, last_visted datetime``

- 管理画面で登録されたRSSを格納するテーブル
- 自動更新の際、ここに登録されたRSSを巡回取得し、``last_visited``を更新する


### posts

``id int, rss_id int, url varchar, title varchar, count int, created_at datetime``

- RSSから読み出された記事を格納するテーブル
- トップページでのリンククリック毎に``count``を更新する
