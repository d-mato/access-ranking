<?php
header('Content-Type: application/json; charset=utf-8');

$db = new PDO('sqlite:rss.db');
$data = json_decode(file_get_contents('php://input'));

if ($_SERVER['REQUEST_METHOD'] == 'GET'){
  $statement = $db->query('SELECT * FROM rss');
  $statement->setFetchMode(PDO::FETCH_ASSOC);
  echo json_encode($statement->fetchAll());
}
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  $now = time();
  $sql = 'INSERT INTO rss (rss_url, site_name, created_at) values (:rss_url, :site_name, :created_at)';
  $query = $db->prepare($sql);
  $query->execute([':rss_url'=>$data->rss_url, ':site_name'=>$data->site_name, ':created_at'=>$now]);
  $result['id'] = $db->lastInsertId();
  $result['created_at'] = $now;
  
  echo json_encode($result);
}
/*
if ($_SERVER['REQUEST_METHOD'] == 'PUT'){
  $sql = 'UPDATE tasks SET done = :done WHERE id = :id';
  $query = $db->prepare($sql);
  $query->execute(array(':done'=>$data->done, ':id'=>$data->id));
}
 */
if ($_SERVER['REQUEST_METHOD'] == 'DELETE'){
  $sql = 'DELETE FROM rss WHERE id = :id';
  $query = $db->prepare($sql);
  $query->execute(array(':id'=>$_GET['id']));
}
