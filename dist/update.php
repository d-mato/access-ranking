<?php
/*
 * 登録されたRSSを取得し、記事DBを更新するスクリプト
 *
 */

$db = new PDO('sqlite:'.dirname(__FILE__).'/rss.db');
$statement = $db->query('SELECT * FROM rss');
$statement->setFetchMode(PDO::FETCH_ASSOC);
$rss_list = $statement->fetchAll();

print_r($rss_list);

foreach ($rss_list as $rss) {
  $url = $rss['rss_url'];
  $rss_id = $rss['id'];
  
  try {
    $doc = @file_get_contents($url);
    $obj = @simplexml_load_string($doc);
 
    foreach ($obj->channel->item as $item) {
      $x = [];
      $x['link'] = (string)$item->link;
      $x['title'] = (string)$item->title;
      $x['description'] = (string)$item->description;
      $x['pubDate'] = strtotime((string)$item->pubDate);
    
      $sql = 'INSERT INTO posts (rss_id, url, title, pub_date, created_at) values (:rss_id, :url, :title, :pub_date, :created_at)';
      $query = $db->prepare($sql);
      $query->execute([
        ':rss_id'     => $rss_id,
        ':url'        => $x['link'],
        ':title'      => $x['title'],
        ':pub_date'   => $x['pubDate'],
        ':created_at' => time()
      ]);
    }
  } catch (Exception $e) {
    continue;
  }
}
