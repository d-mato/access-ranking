<?php
/*
 * 与えられたURLのRSSを取得し、jsonに変換して返すスクリプト
 */
header('Content-Type: application/json; charset=utf-8');

try {
  $url = $_GET['url'];
  if (!preg_match("/^http/", $url)) throw new Exception('Invalid url');

  $doc = file_get_contents($url);
  if (!preg_match("/^<\?xml/", $doc)) throw new Exception('Invalid format');

  $obj = simplexml_load_string($doc);

  $title = (string)$obj->channel->title;
  $description = (string)$obj->channel->description;
  if ($title == '') $title = "(title: unknown)";
  if ($description == '') $description = "(description: unknown)";

  echo $json = json_encode(['title'=>$title, 'description'=>$description]);

} catch (Exception $e) {
  echo "{success: false}";
}

