class RssModel extends Backbone.Model
  url: ->
    "api.php?id=#{@id}"
  defaults:
    rss_url: null
    site_name: null
    created_at: null
    last_visited: null

class RssView extends Backbone.View
  tagName: 'tr'
  events:
    'click .del': 'onClickDel'
  initialize: ->
    @render()
  template: _.template $('#tmpl-rss').html()
  render: ->
    json = @model.toJSON()
    d = new Date(json.created_at*1000)
    json.created_at = d.toLocaleString()
    $(@el).html @template(json)
  onClickDel: ->
    if window.confirm 'Delete RSS ?'
      @model.destroy()

class RssList extends Backbone.Collection
  model: RssModel
  url: 'api.php'

class RssListView extends Backbone.View
  el: '#table-rss tbody'

  initialize: (options) ->
    @models = options.collection
    @models.on 'sync destroy', @render, @

  render: ->
    $(@el).html ''
    @models.each (model) =>
      rssView = new RssView({model: model})
      $(@el).append(rssView.el)

class PreviewModel extends Backbone.Model
  fetch: (url) ->
    @trigger 'fetching'
    $.get('get_rss.php', {url: encodeURI(url)}, (json) =>
      # obj = {title: "サイトタイトル", description: "テストサイトです"}
      try
        @set json
      catch e
        @trigger 'error', e
    ).fail =>
      @trigger 'error'

class PreView extends Backbone.View
  el: '#preview'
  initialize: ->
    @model.on 'fetching', @renderLoading, @
    @model.on 'change', @render, @
    @model.on 'error', =>
      alert '入力されたURLが正しいか確認してください。'
      @render()
  tmpl: _.template('<h2><%=title%></h2><p><%=description%></p>')
  renderLoading: ->
    $(@el).html '<p>loading ...</p>'
  render: ->
    if @model.has 'title'
      $(@el).html @tmpl (@model.toJSON())
      $(@el).addClass 'on'
    else
      $(@el).html ''
      $(@el).removeClass 'on'

preview = new PreviewModel()
new PreView {model: preview}

rssList = new RssList()
new RssListView {collection: rssList}

rssList.fetch()

preview.on 'fetching', ->
  $('#rss-url').prop 'disabled', true
  $('#btn-check').prop 'disabled', true
preview.on 'error change', ->
  $('#rss-url').prop 'disabled', false
  $('#btn-check').prop 'disabled', false

$('#check-rss').submit (e) ->
  e.preventDefault()
  url = $('#rss-url').val()
  return false unless url
  $('#rss-url').val()

  preview.clear()
  preview.fetch(url)

$('#add-rss').click ->
  url = $('#rss-url').val()
  rss = rssList.create {rss_url: url, site_name: preview.get 'title'}
  preview.clear()

$('#cancel').click ->
  preview.clear()
