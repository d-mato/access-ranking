<?php
/*
 * ログイン状態であればRSS設定画面を表示
 * 未ログイン状態であればログインフォームを表示
 */

$PASSWORD = '1234';//パスワード設定

session_start();
function login_state() {
  return isset($_SESSION['login']);
}

if (isset($_POST['password']) && is_string($_POST['password'])) {
  if ($_POST['password'] === $PASSWORD) {
    $_SESSION['login'] = true;
    session_regenerate_id(true);
  }
}
?>
<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php if (login_state()): ?>
<div class="container">

  <div class="row"> 
    <form id="check-rss">
      <div class="input-group">
        <input type="url" placeholder="url" id="rss-url" class="form-control" required>
        <span class="input-group-btn">
          <button type="submit" class="btn btn-default" id="btn-check">check</button>
        </span>
      </div>
    </form>
  </div>
  
  <div class="row"> 
    <div id="preview" class=""></div>
    <div id="btn-group-preview">
      <button id="add-rss" class="btn btn-primary">Add</button>
      <button id="cancel" class="btn btn-default">Cancel</button>
    </div>
  </div>

  <div class="row"> 
    <table id="table-rss" class=" table table-striped">
      <thead><tr>
        <td>rss_url</td>
        <td>site_name</td>
        <td>created_at</td>
        <td></td>
      </tr></thead>
      <tbody></tbody>
    </table>
  </div>

</div>

<script type="text/template" id="tmpl-rss">
<td><%=rss_url%></td>
<td><%=site_name%></td>
<td><%=created_at%></td>
<td><span class="del glyphicon glyphicon-remove" aria-hidden="true" title="delete rss"></span></td>
</script>

<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.2.3/backbone-min.js"></script>
<script src="js/coffee-script.js"></script>

<script src="js/admin.coffee" type="text/coffeescript"></script>

<?php else: ?>
<div class="container">
  <div class="row login">
    <h2>Please log in</h2>
    <form action="admin.php" method="post">
      <input type="password" name="password" placeholder="password" class="form-control">
      <button class="btn btn-primary btn-block" type="submit">Log in</button>
    </form>
  </div>
</div>
<?php endif; ?>
</body>
</html>
