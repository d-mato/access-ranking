<?php
/*
 * 記事リンクのクリック回数をカウントし、DBを更新するスクリプト 
 * 更新後にリダイレクトさせる
 */
try {
  $url = $_GET['url'];
  $db = new PDO('sqlite:rss.db');

  $sql = 'UPDATE posts SET count=count+1 where url = :url';
  $query = $db->prepare($sql);
  $query->execute([':url'=>$url]);

  header("location: {$_GET['url']}");
} catch (Exception $e) {
  header("location: {$_GET['url']}");
}

