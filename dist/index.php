<?php

$db = new PDO('sqlite:rss.db');

$sql = "SELECT * FROM posts inner join rss on posts.rss_id = rss.id order by pub_date desc";

$statement = $db->query($sql);
$statement->setFetchMode(PDO::FETCH_ASSOC);
$posts = $statement->fetchAll();
// print_r($posts);
?>
<!doctype html>
<html lang="ja">
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container">

<ul>
<?php foreach ($posts as $post): ?>
  <li>[click: <?=$post['count']?>] <a href="redirect.php?url=<?=$post['url']?>" target=_blank><?=$post['title']?></a> - <?=$post['site_name']?></li>
<?php endforeach; ?>
</ul>
