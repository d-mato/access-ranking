PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE rss (id integer primary key autoincrement, rss_url char not null, site_name char, created_at int not null, last_visited int);
CREATE TABLE posts (id integer primary key, rss_id int not null, url char, title char, pub_date int, count int default 0, created_at int, unique(rss_id, url) );
COMMIT;
